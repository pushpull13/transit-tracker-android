package com.example.busee

import android.app.Application
import com.example.busee.presentation.atlas.AtlasViewModel
import com.example.busee.source.TransitTrackerApi
import com.example.busee.utils.dataStore.DataStoreInstance
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.core.context.startKoin
import org.koin.dsl.module


class BaseApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@BaseApplication)

            val dataStore = DataStoreInstance(context = this@BaseApplication)
//            val source : AbstractSource = Source(context = this@BaseApplication)
            val transitTrackerApi  = TransitTrackerApi(context = this@BaseApplication)

            modules(
                module {

                    single { dataStore }
                    single { transitTrackerApi }
                }
            )
        }
    }
}

