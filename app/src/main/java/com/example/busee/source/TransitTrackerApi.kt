package com.example.busee.source

import android.content.Context
import android.util.Log
import com.example.busee.model.Agency
import com.example.busee.model.AgencyResponse
import com.example.busee.model.AgencyVehicleResponse
import com.example.busee.model.Region
import com.example.busee.model.RegionResponse
import com.example.busee.model.RelatedTripResponse
import com.example.busee.model.SingleRegionResponse
import com.example.busee.model.Trip
import com.example.busee.model.Vehicle
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.engine.cio.CIO
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.get
import io.ktor.client.request.header
import io.ktor.client.request.parameter
import io.ktor.client.request.url
import io.ktor.serialization.kotlinx.json.json
import kotlinx.serialization.json.Json


class TransitTrackerApi(private val context: Context) {

    private val client = HttpClient(CIO) {
//        install(HttpCache) {
//            publicStorage(FileStorage(context.cacheDir))
//        }
        install(ContentNegotiation) {
            json(
                Json {
                    ignoreUnknownKeys = true
                    isLenient = true
                    encodeDefaults = true
                }
            )
        }
    }

    suspend fun getAgencies(): AgencyResponse {
//        https://api.transittracker.ca/v2/agencies
        val httpRequestBuilder = HttpRequestBuilder()
            .apply {
                url("https://api.transittracker.ca/v2/agencies")
                header("Content-Type", "application/json")
                header("Accept", "application/json")
                header("Accept-Language", "en")
            }
        try {
            val httpResponse = client.get(httpRequestBuilder)
            return httpResponse.body<AgencyResponse>()
        } catch (e: Exception) {
            e.printStackTrace()
            return AgencyResponse(emptyList())
        }
    }

    suspend fun getAllRegions(): List<Region> {
        Log.d(TAG, "getAllRegions")
        val httpRequestBuilder = HttpRequestBuilder()
            .apply {
                url("https://api.transittracker.ca/v2/regions")
                header("Content-Type", "application/json")
                header("Accept", "application/json")
                header("Accept-Language", "en")
            }

        try {
            val httpResponse = client.get(httpRequestBuilder)
            val regionResponse = httpResponse.body<RegionResponse>()
            return regionResponse.regions
        } catch (e: Exception) {
            e.printStackTrace()
            return emptyList()
        }
    }

    /**
     * Get region and its agencies
     * @param regionSlug Toronto, "tor"
     */
    suspend fun getRegion(regionSlug: String): Region? {
        Log.d(TAG, "getRegion: $regionSlug")

        val httpRequestBuilder = HttpRequestBuilder()
            .apply {
                url("https://api.transittracker.ca/v2/regions/$regionSlug")
                header("Content-Type", "application/json")
                header("Accept", "application/json")
                header("Accept-Language", "en")
            }

        try {
            val httpResponse = client.get(httpRequestBuilder)
            val singleRegionResponse = httpResponse.body<SingleRegionResponse>()
            return singleRegionResponse.region
        } catch (e: Exception) {
            e.printStackTrace()
            return null
        }
    }

    suspend fun getVehiclesInAgency(agency: Agency): List<Vehicle> {
        val httpRequestBuilder = HttpRequestBuilder()
            .apply {
                url("https://api.transittracker.ca/v2/agencies/${agency.slug}/vehicles")
                header("Content-Type", "application/json")
                header("Accept", "application/json")
                header("Accept-Language", "en")
                parameter("geojson", "false")
            }

        try {
            val httpResponse = client.get(httpRequestBuilder)
            val agencyVehicleResponse = httpResponse.body<AgencyVehicleResponse>()
            return agencyVehicleResponse.vehicleList
        } catch (e: Exception) {
            e.printStackTrace()
            return emptyList()
        }
    }

    suspend fun getTripShape(agencySlug: String, shapeId: String): String {
        Log.d(TAG, "getTripShape: $agencySlug, $shapeId")
        val httpRequestBuilder = HttpRequestBuilder()
            .apply {
                url("https://api.transittracker.ca/v2/agencies/$agencySlug/shapes/$shapeId")
                header("Content-Type", "application/json")
                header("Accept", "application/json")
                header("Accept-Language", "en")
            }

        try {
            val httpResponse = client.get(httpRequestBuilder)
            val geoJsonData = httpResponse.body<String>()
            return geoJsonData
        } catch (e: Exception) {
            e.printStackTrace()
            return ""
        }
    }

    suspend fun getRelatedTrips(agencySlug: String, tripId: String): List<Trip> {
        Log.d(TAG, "getRelatedTrips: $agencySlug, $tripId")
        val httpRequestBuilder = HttpRequestBuilder()
            .apply {
                url("https://api.transittracker.ca/v2/agencies/$agencySlug/trips/$tripId/blocks")
                header("Content-Type", "application/json")
                header("Accept", "application/json")
                header("Accept-Language", "en")
            }

        try {
            val httpResponse = client.get(httpRequestBuilder)
            val tripList = httpResponse.body<RelatedTripResponse>().data
            return tripList

        } catch (e: Exception) {
            e.printStackTrace()
            return emptyList()
        }
    }

    companion object {
        private const val TAG = "Source"
    }
}
