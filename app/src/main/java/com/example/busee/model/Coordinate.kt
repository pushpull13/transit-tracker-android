package com.example.busee.model

import com.mapbox.geojson.Point
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


@Serializable
data class Coordinate(
    @SerialName("lat") val lat: Double,
    @SerialName("lon") val lon: Double
) {
    val point : Point
        get() = Point.fromLngLat(lon, lat)
}
