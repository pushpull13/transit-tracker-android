package com.example.busee.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


@Serializable
data class Agency(
    @SerialName("id") val id: Int,
    @SerialName("isArchived") val isArchived: Boolean,
    @SerialName("name") val name: String,
    @SerialName("shortName") val shortName: String,
    @SerialName("slug") val slug: String,
    @SerialName("cities") val cities: List<String>,
    @SerialName("defaultVehicleType") val defaultVehicleType: String,
    @SerialName("color") val color: String,
    @SerialName("textColor") val textColor: String,
    @SerialName("regions") val regions: List<String>,
    @SerialName("license") val license: License,
    @SerialName("features") val features: List<String>,
    @SerialName("meta") val meta: Map<String, String>
)

@Serializable
data class AgencyResponse(
    @SerialName("data") val agencies: List<Agency>
)
