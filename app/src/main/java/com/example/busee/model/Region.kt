package com.example.busee.model

import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.SerializationStrategy
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.builtins.serializer
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.descriptors.listSerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.encoding.encodeCollection


@Serializable
data class Region(
    @SerialName("name") val name: String,
    @SerialName("slug") val slug: String,
    @SerialName("credits") val credits: String,
    @SerialName("infoTitle") val infoTitle: String,
    @SerialName("infoBody") val infoBody: String,
    @SerialName("description") val description: String,
    @SerialName("metaDescription") val metaDescription: String,
    @SerialName("cities") val cityList: List<String>,
    @SerialName("image") val image: String,
    @SerialName("mapBox") @Serializable(MapBoundSerializer::class) val mapBound: Pair<Coordinate, Coordinate>,
    @SerialName("mapCenter") val mapCenter: Coordinate,
    @SerialName("mapZoom") val mapZoom: Double,
    @SerialName("agencies") val agencyList: List<Agency>
)

@Serializable
data class RegionResponse(
    @SerialName("data") val regions: List<Region>
)

@Serializable
data class SingleRegionResponse(
    @SerialName("data") val region: Region
)

object MapBoundSerializer : KSerializer<Pair<Coordinate, Coordinate>> {
    @OptIn(ExperimentalSerializationApi::class)
    override val descriptor: SerialDescriptor = listSerialDescriptor<List<List<Coordinate>>>()

    override fun serialize(encoder: Encoder, value: Pair<Coordinate, Coordinate>) {
        val (first, second) = value
        encoder.encodeCollection(descriptor, 2) {
            encodeSerializableElement(descriptor, 0, ListSerializer(Double.serializer()), listOf(first.lat, first.lon))
            encodeSerializableElement(descriptor, 1, ListSerializer(Double.serializer()), listOf(second.lat, second.lon))
        }
    }

    override fun deserialize(decoder: Decoder): Pair<Coordinate, Coordinate> {
        val list = decoder.decodeSerializableValue(ListSerializer(ListSerializer(Double.serializer())))
        val first = list[0]
        val second = list[1]
        return Pair(Coordinate(first[0], first[1]), Coordinate(second[0], second[1]))
    }
}
