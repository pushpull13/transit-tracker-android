package com.example.busee.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


@Serializable
data class License(
    @SerialName("url") val url: String?,
    @SerialName("title") val title: String,
    @SerialName("isDownloadable") val isDownloadable: Boolean
)
