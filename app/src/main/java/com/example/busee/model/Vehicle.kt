package com.example.busee.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


@Serializable
data class Vehicle(
    @SerialName("id") val id: Int,
    @SerialName("reference") val reference: Int? = null,
    @SerialName("isActive") val isActive: Boolean,
    @SerialName("label") val label: String,
    @SerialName("timestamp") val timestamp: Long,
    @SerialName("tripId") val tripId: String? = null,
    @SerialName("routeId") val routeId: String,
    @SerialName("startTimestamp") val startTimestamp: Long? = null,
    @SerialName("position") val location: Coordinate,
    @SerialName("bearing") val bearing: Double?,
    @SerialName("speed") val speed: Double?,
    @SerialName("vehicleType") val vehicleType: String,
    @SerialName("plate") val plate: String?,
    @SerialName("odometer") val odometer: Double?,
    @SerialName("fuelLevel") val currentStop: String? = null,
    @SerialName("currentStatus") val currentStatus: LabelData?,
    @SerialName("scheduleRelationship") val scheduleRelationship: LabelData?,
    @SerialName("congestionLevel") val congestionLevel: LabelData?,
    @SerialName("occupancyStatus") val occupancyStatus: LabelData?,
    @SerialName("agency") val agencySlug: String,
    @SerialName("links") val links: List<Int>,
    @SerialName("tags") val tags: List<Int>,
    @SerialName("trip") val trip: Trip,
    @SerialName("createdAt") val createdAt: String,
    val feature: GeoJsonFeature? = null,
) {
    val isBus: Boolean
        get() = vehicleType == "bus"
}

@Serializable
data class LabelData(
    @SerialName("data") val data : String?,
    @SerialName("label") val label : String?,
)

@Serializable
data class AgencyVehicleResponse(
    @SerialName("data") val vehicleList: List<Vehicle> = emptyList(),
    @SerialName("timestamp") val timestamp: Long = 0,
    @SerialName("count") val count: Int = 0,
    @SerialName("geojson") val geojsonData: GeoJsonData? = null,
)

@Serializable
data class GeoJsonData(
    @SerialName("type") val type: String,
    @SerialName("features") val features: List<GeoJsonFeature>,
)

@Serializable
data class GeoJsonFeature(
    @SerialName("type") val type: String,
    @SerialName("properties") val properties: GeoJsonDataProperties,
    @SerialName("geometry") val geometry: GeoJsonGeometry,
)

@Serializable
data class GeoJsonDataProperties(
    @SerialName("id") val id: Int,
    @SerialName("label") val label: String,
    @SerialName("marker-symbol") val markerSymbol: String,
)

@Serializable
data class GeoJsonGeometry(
    @SerialName("type") val type: String,
    @SerialName("coordinates") val coordinates: List<Double>,
)

data class AgencyVehicleData(
    val agency : Agency,
    val vehicleList: List<Vehicle>,
)
