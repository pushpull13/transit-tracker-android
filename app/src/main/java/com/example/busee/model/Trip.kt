package com.example.busee.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


@Serializable
data class Trip(
    @SerialName("id") val id: String? = null,
    @SerialName("headsign") val headSign: String? = null,
    @SerialName("shortName") val shortName: String? = null,
    @SerialName("routeColor") val routeColor: String? = null,
    @SerialName("routeTextColor") val routeTextColor: String? = null,
    @SerialName("routeShortName") val routeShortName: String? = null,
    @SerialName("routeLongName") val routeLongName: String? = null,
    @SerialName("shapeLink") val shapeLink: String? = null,
    @SerialName("shapeId") val shapeId: String? = null,
    @SerialName("serviceId") val serviceId: String? = null,
    @SerialName("blockId") val blockId: String? = null,
)

@Serializable
data class RelatedTripResponse(
    @SerialName("data") val data: List<Trip> = emptyList(),
)
