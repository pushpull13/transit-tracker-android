package com.example.busee.utils.dataStore

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.mapLatest
import kotlinx.coroutines.launch
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json


val Context.dataStore: DataStore<Preferences> by preferencesDataStore("dataStore")

class DataStoreInstance(private val context: Context) {
    companion object {
        private val IS_FIRST_TIME = booleanPreferencesKey("is_first_time")
        private val STORED_VERSION = intPreferencesKey("stored_version")
        private val PREFERENCE_FOLLOW_SYSTEM_DARK_THEME = booleanPreferencesKey("follow_system_dark_theme")
        private val PREFERENCE_FORCE_DARK_THEME = booleanPreferencesKey("force_dark_theme")
        private val PREFERENCE_DARK_THEME = stringPreferencesKey("dark_theme")

        private val PREFERENCE_REGION = stringPreferencesKey("region")
    }

    private val json = Json

    fun clearDatastore() = CoroutineScope(Dispatchers.IO).launch {
        context.dataStore.edit { pref -> pref.clear() }
    }

    val getIsFirstTime: Flow<Boolean> =
        context.dataStore.data.map { preferences -> preferences[IS_FIRST_TIME] ?: true }

    fun putIsFirstTime(isFirstTime: Boolean) = CoroutineScope(Dispatchers.IO).launch {
        context.dataStore.edit { pref -> pref[IS_FIRST_TIME] = isFirstTime }
    }

    val storedVersion: Flow<Int> =
        context.dataStore.data.map { preferences -> preferences[STORED_VERSION] ?: 0 }

    fun putStoredVersion(version: Int) = CoroutineScope(Dispatchers.IO).launch {
        context.dataStore.edit { pref -> pref[STORED_VERSION] = version }
    }

    val followSystemDarkTheme: Flow<Boolean> =
        context.dataStore.data.map { preferences -> preferences[PREFERENCE_FOLLOW_SYSTEM_DARK_THEME] ?: true }

    fun putFollowSystemDarkTheme(followSystemDarkTheme: Boolean) = CoroutineScope(Dispatchers.IO).launch {
        context.dataStore.edit { pref -> pref[PREFERENCE_FOLLOW_SYSTEM_DARK_THEME] = followSystemDarkTheme }
    }

    val forceDarkTheme: Flow<Boolean> =
        context.dataStore.data.map { preferences -> preferences[PREFERENCE_FORCE_DARK_THEME] ?: false }

    fun putForceDarkTheme(forceDarkTheme: Boolean) = CoroutineScope(Dispatchers.IO).launch {
        context.dataStore.edit { pref -> pref[PREFERENCE_FORCE_DARK_THEME] = forceDarkTheme }
    }

    val darkTheme: Flow<String> =
        context.dataStore.data.map { preferences -> preferences[PREFERENCE_DARK_THEME] ?: "system" }

    fun putDarkTheme(darkTheme: String) = CoroutineScope(Dispatchers.IO).launch {
        context.dataStore.edit { pref -> pref[PREFERENCE_DARK_THEME] = darkTheme }
    }

    val region: Flow<String> =
        context.dataStore.data.map { preferences -> preferences[PREFERENCE_REGION] ?: ""}

    fun putRegion(region: String) = CoroutineScope(Dispatchers.IO).launch {
        context.dataStore.edit { pref -> pref[PREFERENCE_REGION] = region }
    }
}
