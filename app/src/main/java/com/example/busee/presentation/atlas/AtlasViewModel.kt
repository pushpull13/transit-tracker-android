package com.example.busee.presentation.atlas

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.busee.model.AgencyVehicleData
import com.example.busee.model.Region
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import org.koin.android.annotation.KoinViewModel


@KoinViewModel
class AtlasViewModel(
) : ViewModel() {

    init {
        Log.d(TAG, "AtlasViewModel: init")
    }

    private val _regionListFlow: MutableStateFlow<List<Region>> = MutableStateFlow(emptyList())
    val regionListFlow = _regionListFlow.asStateFlow()

    private val _vehicleListFlow: MutableStateFlow<List<AgencyVehicleData>> = MutableStateFlow(emptyList())
    val vehicleListFlow = _vehicleListFlow.asStateFlow()



    companion object {
        private const val TAG = "AtlasViewModel"
    }
}
