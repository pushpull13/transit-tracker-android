package com.example.busee.presentation.composable

import android.content.Intent
import android.widget.Toast
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredSize
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.example.busee.R
import com.example.busee.model.Agency
import com.example.busee.model.AgencyResponse
import com.example.busee.source.TransitTrackerApi
import com.google.android.gms.oss.licenses.OssLicensesMenuActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.koin.compose.koinInject


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun InfoView(
    onClickBack: () -> Unit
) {
    val context = LocalContext.current
    val uriHandler = LocalUriHandler.current

    val transitTrackerApi: TransitTrackerApi = koinInject()
    var agencyResponse by remember { mutableStateOf<AgencyResponse?>(null) }
    LaunchedEffect(Unit) {
        withContext(Dispatchers.IO) {
            transitTrackerApi.getAgencies().let { response ->
                withContext(Dispatchers.Main) {
                    agencyResponse = response
                }
            }
        }
    }

    Scaffold(
        topBar = {
            TopAppBar(
                navigationIcon = {
                    IconButton(
                        onClick = onClickBack
                    ) {
                        Icon(painter = painterResource(id = R.drawable.ic_close), contentDescription = stringResource(id = R.string.close))
                    }
                },
                title = {
                    Text(
                        text = stringResource(id = R.string.info)
                    )
                }
            )
        }
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(it)
                .verticalScroll(rememberScrollState())
        ) {
            InfoButton(
                icon = R.drawable.ic_gitlab,
                title = stringResource(id = R.string.source_code),
                subtitle = stringResource(id = R.string.app_source_code),
                onClick = {
                    try {
                        uriHandler.openUri("https://gitlab.com/pushpull13/transit-tracker-android")
                    } catch (e: Exception) {
                        e.printStackTrace()
                        Toast.makeText(context, context.resources.getText(R.string.error_opening_url), Toast.LENGTH_SHORT).show()
                    }
                }
            )
            InfoButton(
                icon = R.drawable.ic_api,
                title = "Transit Tracker API",
                subtitle = stringResource(id = R.string.transit_data_provider),
                onClick = {
                    try {
                        uriHandler.openUri("https://www.transittracker.ca/")
                    } catch (e: Exception) {
                        e.printStackTrace()
                        Toast.makeText(context, context.resources.getText(R.string.error_opening_url), Toast.LENGTH_SHORT).show()
                    }
                }
            )
            InfoButton(
                icon = R.drawable.ic_map,
                title = "MapLibre",
                subtitle = stringResource(id = R.string.map_renderer),
                onClick = {
                    try {
                        uriHandler.openUri("https://maplibre.org/")
                    } catch (e: Exception) {
                        e.printStackTrace()
                        Toast.makeText(context, context.resources.getText(R.string.error_opening_url), Toast.LENGTH_SHORT).show()
                    }
                }
            )
            InfoButton(
                icon = R.drawable.ic_map_legend,
                title = "Map Tiler",
                subtitle = stringResource(id = R.string.map_data_provider),
                onClick = {
                    try {
                        uriHandler.openUri("https://www.maptiler.com/")
                    } catch (e: Exception) {
                        e.printStackTrace()
                        Toast.makeText(context, context.resources.getText(R.string.error_opening_url), Toast.LENGTH_SHORT).show()
                    }
                }
            )
            InfoButton(
                icon = R.drawable.ic_shape,
                title = "Pictogrammers",
                subtitle = stringResource(id = R.string.icon_library),
                onClick = {
                    try {
                        uriHandler.openUri("https://pictogrammers.com/library/mdi//")
                    } catch (e: Exception) {
                        e.printStackTrace()
                        Toast.makeText(context, context.resources.getText(R.string.error_opening_url), Toast.LENGTH_SHORT).show()
                    }
                }
            )
            InfoButton(
                icon = R.drawable.ic_code_braces,
                title = stringResource(id = R.string.open_source_licenses),
                onClick = {
                    context.startActivity(Intent(context, OssLicensesMenuActivity::class.java))
                }
            )
            agencyResponse?.agencies?.forEach {
                AgencyLicense(agency = it) {
                    it.license.url?.let { url ->
                        try {
                            uriHandler.openUri(url)
                        } catch (e: Exception) {
                            e.printStackTrace()
                            Toast.makeText(context, context.resources.getText(R.string.error_opening_url), Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }
        }
    }
}

@Composable
private fun InfoButton(
    icon: Int,
    title: String,
    subtitle: String? = null,
    onClick: () -> Unit
) {
    Row(
        verticalAlignment = androidx.compose.ui.Alignment.CenterVertically,
        modifier = Modifier
            .fillMaxSize()
            .heightIn(min = 72.dp)
            .clickable { onClick() }
    ) {
        Spacer(modifier = Modifier.width(24.dp))
        Icon(
            painter = painterResource(id = icon),
            contentDescription = title,
            modifier = Modifier.requiredSize(32.dp)
        )
        Spacer(modifier = Modifier.width(24.dp))
        Column {
            Text(text = title, style = MaterialTheme.typography.titleMedium)
            subtitle?.let { Text(text = subtitle, style = MaterialTheme.typography.bodySmall) }
        }
        Spacer(modifier = Modifier.weight(1f))
        Icon(
            painter = painterResource(id = R.drawable.ic_sr_arrow_left),
            contentDescription = title,
            modifier = Modifier
                .requiredSize(24.dp)
                .graphicsLayer { rotationZ = 180f }
        )
        Spacer(modifier = Modifier.width(24.dp))
    }
}

@Composable
private fun AgencyLicense(
    agency: Agency,
    onClick: () -> Unit
) {
    Row(
        verticalAlignment = androidx.compose.ui.Alignment.CenterVertically,
        modifier = Modifier
            .fillMaxSize()
            .heightIn(min = 72.dp)
            .clickable { onClick() }
    ) {
        Spacer(modifier = Modifier.width(24.dp))
        Icon(
            painter = painterResource(id = R.drawable.ic_bus),
            contentDescription = stringResource(id = R.string.agency),
            modifier = Modifier.requiredSize(32.dp)
        )
        Spacer(modifier = Modifier.width(24.dp))
        Column {
            Text(text = agency.name, style = MaterialTheme.typography.titleMedium)
            Text(text = agency.license.title, style = MaterialTheme.typography.bodySmall)
        }
        Spacer(modifier = Modifier.weight(1f))
        Icon(
            painter = painterResource(id = R.drawable.ic_sr_arrow_left),
            contentDescription = stringResource(id = R.string.agency),
            modifier = Modifier
                .requiredSize(24.dp)
                .graphicsLayer { rotationZ = 180f }
        )
        Spacer(modifier = Modifier.width(24.dp))
    }
}
