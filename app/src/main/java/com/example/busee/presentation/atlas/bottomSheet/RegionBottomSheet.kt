package com.example.busee.presentation.atlas.bottomSheet

import android.widget.TextView
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.requiredSize
import androidx.compose.foundation.layout.width
import androidx.compose.material3.BottomSheetDefaults
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ModalBottomSheet
import androidx.compose.material3.SheetState
import androidx.compose.material3.Text
import androidx.compose.material3.contentColorFor
import androidx.compose.material3.rememberModalBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.core.text.HtmlCompat
import com.example.busee.R
import com.example.busee.model.Region
import com.example.busee.presentation.composable.genericBottomSheet2.GenericBottomSheet2
import com.example.busee.presentation.composable.genericBottomSheet2.GenericBottomSheetSkeleton2
import com.example.busee.presentation.theme.ICON_SIZE


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun RegionBottomSheet(
    bottomSheetState: SheetState = rememberModalBottomSheetState(),
    isBottomSheetVisible: Boolean = false,
    onDismissRequest: () -> Unit = { },
    region: Region,
    onClickChangeRegion: () -> Unit
) {

    val contentColor = contentColorFor(backgroundColor = BottomSheetDefaults.ContainerColor)

    GenericBottomSheet2(
        bottomSheetState = bottomSheetState,
        isBottomSheetVisible = isBottomSheetVisible,
        onDismissRequest = onDismissRequest,
        scrimColor = Color.Transparent
    ) {
        GenericBottomSheetSkeleton2(
            title = {
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    verticalAlignment = androidx.compose.ui.Alignment.CenterVertically
                ) {
                    Text(
                        text = region.name,
                        style = MaterialTheme.typography.headlineSmall,
                        fontWeight = FontWeight.Bold,
                    )
                    Spacer(modifier = Modifier.weight(1f))
                    Button(onClick = onClickChangeRegion) {
                        Text(
                            text = stringResource(id = R.string.change_region),
                        )
                        Spacer(modifier = Modifier.width(8.dp))
                        Icon(
                            painter = painterResource(id = R.drawable.ic_sr_swap),
                            contentDescription = stringResource(id = R.string.change_region),
                            modifier = Modifier.requiredSize(ICON_SIZE)
                        )
                    }
                }
            }

        ) {
            Text(
                text = region.infoTitle,
                style = MaterialTheme.typography.bodyMedium
            )
            val spannedText = HtmlCompat.fromHtml(region.infoBody, 0)
            AndroidView(
                modifier = Modifier.fillMaxWidth(),
                factory = { context ->
                    TextView(context).apply {
                        text = spannedText
                        this.setTextColor(contentColor.toArgb())
                    }
                }
            )
        }
    }
}
