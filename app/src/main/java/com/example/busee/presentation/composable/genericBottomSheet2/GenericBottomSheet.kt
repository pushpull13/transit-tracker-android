package com.example.busee.presentation.composable.genericBottomSheet2

import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.material3.BottomSheetDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ModalBottomSheet
import androidx.compose.material3.ModalBottomSheetProperties
import androidx.compose.material3.SheetState
import androidx.compose.material3.contentColorFor
import androidx.compose.material3.rememberModalBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.window.SecureFlagPolicy


@OptIn(ExperimentalMaterial3Api::class)
@Preview
@Composable
fun GenericBottomSheet2(
	modifier: Modifier = Modifier,
	bottomSheetState: SheetState = rememberModalBottomSheetState(),
	isBottomSheetVisible: Boolean = false,
	containerColor: Color = BottomSheetDefaults.ContainerColor,
	contentColor: Color = contentColorFor(containerColor),
	tonalElevation: Dp = BottomSheetDefaults.Elevation,
	scrimColor: Color = Color.Black.copy(alpha = 0.42f),
	onDismissRequest: () -> Unit = { },
	content: @Composable ColumnScope.() -> Unit = { },
) {
	if (isBottomSheetVisible) {
		ModalBottomSheet(
			sheetState = bottomSheetState,
			onDismissRequest = onDismissRequest,
			containerColor = containerColor,
			contentColor = contentColor,
			tonalElevation = tonalElevation,
			scrimColor = scrimColor,
			modifier = modifier,
			content = content,
			properties = ModalBottomSheetProperties(
				securePolicy = SecureFlagPolicy.Inherit,
				isFocusable = true,
				shouldDismissOnBackPress = false
			)
		)
	}
}
