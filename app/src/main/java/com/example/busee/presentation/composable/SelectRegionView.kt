package com.example.busee.presentation.composable

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredSize
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.example.busee.R
import com.example.busee.model.Region
import com.example.busee.presentation.theme.ICON_SIZE


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SelectRegionView(
    regionList: List<Region>,
    onRegionSelected: (Region) -> Unit
) {
    Scaffold(
        topBar = {
            TopAppBar(title = { Text(text = "Select region") })
        }
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(it)
        ) {
            regionList.forEach { region ->
                RegionItem(
                    region = region,
                    onRegionSelected = { onRegionSelected(region) }
                )
            }
        }
    }
}

@Composable
private fun RegionItem(
    region: Region,
    onRegionSelected: () -> Unit
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .fillMaxWidth()
            .clickable { onRegionSelected() }
            .padding(16.dp)
    ) {
        Text(text = region.name)
        Spacer(modifier = Modifier.weight(1f))
        Icon(
            painter = painterResource(id = R.drawable.ic_sr_arrow_left),
            contentDescription = "Select region",
            modifier = Modifier
                .requiredSize(ICON_SIZE)
                .graphicsLayer { rotationZ = 180f }
        )
    }
}
