package com.example.busee.presentation.atlas

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredHeight
import androidx.compose.foundation.layout.requiredSize
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.VerticalDivider
import androidx.compose.material3.rememberModalBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.example.busee.R
import com.example.busee.model.Region
import com.example.busee.presentation.atlas.bottomSheet.RegionBottomSheet
import com.example.busee.presentation.theme.ICON_SIZE
import com.example.busee.utils.dataStore.DataStoreInstance
import kotlinx.coroutines.launch
import org.koin.compose.koinInject


@Composable
fun AtlasOverlay(
    visible: Boolean,
    regions: Region?,
    vehicleType: AtlasDataHolder.Companion.VehicleType = AtlasDataHolder.Companion.VehicleType.Both,
    onVehicleSelected: (AtlasDataHolder.Companion.VehicleType) -> Unit,
    onClickShowInfo: () -> Unit
) {
    AnimatedVisibility(
        visible = visible
    ) {
        Column(
            modifier = Modifier.padding(16.dp)
        ) {
            Spacer(modifier = Modifier.height(64.dp))
            Row(
                modifier = Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically
            ) {
                regions?.let {
                    RegionView(it)
                }
                Spacer(modifier = Modifier.weight(1f))
                VehicleSelector(
                    selectedVehicle = vehicleType,
                    onVehicleSelected = onVehicleSelected
                )
                Spacer(modifier = Modifier.width(16.dp) )
                CheckedSurface(
                    checked = false,
                    shape = RoundedCornerShape(topStart = 12.dp, bottomStart = 12.dp, topEnd = 12.dp, bottomEnd = 12.dp),
                    onCheckedChange = { onClickShowInfo() },
                    modifier = Modifier.requiredSize(40.dp)
                ) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_info),
                        contentDescription = stringResource(id = R.string.info),
                        modifier = Modifier.requiredSize(ICON_SIZE)
                    )
                }
            }
            Spacer(modifier = Modifier.weight(1f))
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun RegionView(region: Region) {
    val scope = rememberCoroutineScope()

    val dataStore: DataStoreInstance = koinInject()

    val bottomSheetState = rememberModalBottomSheetState()
    var isRegionBottomSheetVisible by remember { mutableStateOf(false) }

    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier
            .requiredHeight(40.dp)
            .clip(RoundedCornerShape(12.dp))
            .background(MaterialTheme.colorScheme.surface)
            .clickable { isRegionBottomSheetVisible = true }
    ) {
        Text(
            text = region.name,
            modifier = Modifier.padding(horizontal = 16.dp, vertical = 8.dp)
        )
    }

    RegionBottomSheet(
        bottomSheetState = bottomSheetState,
        isBottomSheetVisible = isRegionBottomSheetVisible,
        onDismissRequest = {
            scope.launch { bottomSheetState.hide() }.invokeOnCompletion {
                if (!bottomSheetState.isVisible) isRegionBottomSheetVisible = false
            }
        },
        region = region,
        onClickChangeRegion = {
            scope.launch { bottomSheetState.hide() }.invokeOnCompletion {
                if (!bottomSheetState.isVisible) isRegionBottomSheetVisible = false
            }
            dataStore.putRegion("")
        }
    )
}

@Composable
private fun VehicleSelector(
    selectedVehicle: AtlasDataHolder.Companion.VehicleType = AtlasDataHolder.Companion.VehicleType.Both,
    onVehicleSelected: (AtlasDataHolder.Companion.VehicleType) -> Unit
) {
    val radius = 12.dp

    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier.background(color = MaterialTheme.colorScheme.surface, shape = RoundedCornerShape(radius))
    ) {
        CheckedSurface(
            checked = selectedVehicle == AtlasDataHolder.Companion.VehicleType.Bus,
            shape = RoundedCornerShape(topStart = radius, bottomStart = radius, topEnd = 0.dp, bottomEnd = 0.dp),
            onCheckedChange = { onVehicleSelected(AtlasDataHolder.Companion.VehicleType.Bus) },
            modifier = Modifier.requiredSize(40.dp)
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_bus),
                contentDescription = stringResource(id = R.string.bus),
                modifier = Modifier.requiredSize(ICON_SIZE)
            )
        }
        VerticalDivider(
            modifier = Modifier
                .height(40.dp)
                .padding(vertical = 8.dp)
        )
        CheckedSurface(
            checked = selectedVehicle == AtlasDataHolder.Companion.VehicleType.Train,
            shape = RectangleShape,
            onCheckedChange = { onVehicleSelected(AtlasDataHolder.Companion.VehicleType.Train) },
            modifier = Modifier.requiredSize(40.dp)
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_train),
                contentDescription = stringResource(id = R.string.train),
                modifier = Modifier.requiredSize(ICON_SIZE)
            )
        }
        VerticalDivider(
            modifier = Modifier
                .height(40.dp)
                .padding(vertical = 8.dp)
        )
        CheckedSurface(
            checked = selectedVehicle == AtlasDataHolder.Companion.VehicleType.Both,
            shape = RoundedCornerShape(topStart = 0.dp, bottomStart = 0.dp, topEnd = radius, bottomEnd = radius),
            onCheckedChange = { onVehicleSelected(AtlasDataHolder.Companion.VehicleType.Both) },
            modifier = Modifier.requiredHeight(40.dp)
        ) {
            Text(
                text = stringResource(id = R.string.both),
                modifier = Modifier.padding(horizontal = 8.dp)
            )
        }
    }
}

@Composable
private fun CheckedSurface(
    modifier: Modifier,
    shape: Shape,
    checked: Boolean,
    onCheckedChange: (Boolean) -> Unit,
    content: @Composable BoxScope.() -> Unit
) {
    val containerColor = if (checked) MaterialTheme.colorScheme.onSurface else MaterialTheme.colorScheme.surface
    val contentColor = if (checked) MaterialTheme.colorScheme.surface else MaterialTheme.colorScheme.onSurface
    Surface(
        checked = checked,
        shape = shape,
        shadowElevation = 0.dp,
        tonalElevation = 0.dp,
        onCheckedChange = onCheckedChange,
        color = containerColor,
        contentColor = contentColor,
        modifier = modifier,
    ) {
        Box(
            modifier = Modifier,
            contentAlignment = Alignment.Center,
            content = content
        )
    }
}
