package com.example.busee.presentation.atlas.bottomSheet

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredSize
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import com.example.busee.R
import com.example.busee.model.Trip
import com.example.busee.model.Vehicle
import com.example.busee.presentation.composable.genericBottomSheet2.BottomSheetKeyText
import com.example.busee.presentation.composable.genericBottomSheet2.GenericBottomSheetInfo
import com.example.busee.presentation.composable.genericBottomSheet2.GenericBottomSheetInfoDefaults


@Composable
fun VehicleBottomSheetContent(vehicle: Vehicle?, relatedTrips: List<Trip>) {
    if (vehicle == null) {
        Surface(
            color = MaterialTheme.colorScheme.errorContainer,
            modifier = Modifier
                .height(164.dp)
                .statusBarsPadding()
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp)
            ) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_alert),
                    contentDescription = null,
                    modifier = Modifier.requiredSize(48.dp)
                )
                Spacer(Modifier.width(16.dp))
                Text(
                    text = "Never use this app for transportation purpose. The information is not always accurate.",
                    style = MaterialTheme.typography.bodyMedium,
                    fontWeight = FontWeight.Bold
                )
            }
        }
    } else {
        Surface(
            modifier = Modifier
                .fillMaxSize()
                .statusBarsPadding()
        ) {
            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier
                    .fillMaxSize()
                    .padding(horizontal = 24.dp)
                    .verticalScroll(rememberScrollState())
            ) {
                Spacer(Modifier.height(12.dp))
                DragHandle()
                Spacer(Modifier.height(12.dp))
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier
                        .fillMaxWidth()
                ) {
                    Icon(
                        painter = if (vehicle.isBus) painterResource(id = R.drawable.ic_bus) else painterResource(id = R.drawable.ic_train),
                        contentDescription = null,
                        modifier = Modifier.requiredSize(72.dp)
                    )
                    Spacer(Modifier.width(16.dp))
                    Column {
                        Text(
                            text = vehicle.label,
                            style = MaterialTheme.typography.headlineSmall,
                            fontWeight = FontWeight.Bold
                        )
                        Text(
                            text = vehicle.trip.headSign ?: "?",
                            style = MaterialTheme.typography.bodyMedium
                        )
                    }
                }
                Spacer(Modifier.height(16.dp))
                Row(
                    modifier = Modifier.fillMaxWidth(),
                ) {
                    GenericBottomSheetInfo(
                        key = stringResource(id = R.string.speed),
                        value = vehicle.speed?.toString() ?: "?",
                        icon = R.drawable.ic_speedometer,
                        colors = GenericBottomSheetInfoDefaults.primaryInfoColors(),
                        modifier = Modifier.weight(1f)
                    )
                    GenericBottomSheetInfo(
                        key = stringResource(id = R.string.status),
                        value = vehicle.currentStatus?.label ?: "?",
                        icon = R.drawable.ic_bus_stop,
                        colors = GenericBottomSheetInfoDefaults.primaryInfoColors(),
                        modifier = Modifier.weight(1f)
                    )
                }
                Row {
                    GenericBottomSheetInfo(
                        key = stringResource(id = R.string.vehicle_id),
                        value = vehicle.id.toString(),
                        icon = if (vehicle.isBus) R.drawable.ic_bus else R.drawable.ic_train,
                        colors = GenericBottomSheetInfoDefaults.primaryInfoColors(),
                        modifier = Modifier.weight(1f)
                    )
                    GenericBottomSheetInfo(
                        key = stringResource(id = R.string.schedule_status),
                        value = vehicle.scheduleRelationship?.label ?: "?",
                        icon = R.drawable.ic_timeline_plus,
                        colors = GenericBottomSheetInfoDefaults.primaryInfoColors(),
                        modifier = Modifier.weight(1f)
                    )
                }
                Row {
                    GenericBottomSheetInfo(
                        key = stringResource(id = R.string.trip_id),
                        value = vehicle.tripId ?: "?",
                        icon = R.drawable.ic_identifier,
                        colors = GenericBottomSheetInfoDefaults.primaryInfoColors(),
                        modifier = Modifier.weight(1f)
                    )
                    GenericBottomSheetInfo(
                        key = stringResource(id = R.string.occupancy),
                        value = vehicle.occupancyStatus?.label ?: "?",
                        icon = R.drawable.ic_seat_passenger,
                        colors = GenericBottomSheetInfoDefaults.primaryInfoColors(),
                        modifier = Modifier.weight(1f)
                    )
                }
                Spacer(Modifier.height(16.dp))
                Text(
                    text = stringResource(id = R.string.related_trips),
                    style = MaterialTheme.typography.titleMedium,
                    fontWeight = FontWeight.Bold,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(start = 8.dp)
                )
                Spacer(Modifier.height(0.dp))
                relatedTrips.forEach { trip ->
                    RelatedTrip(trip)
                }
            }
        }
    }
}

@Composable
private fun RelatedTrip(trip: Trip) {
    val tripColor = try {
        Color(android.graphics.Color.parseColor(trip.routeColor))
    } catch (e: Exception) {
        MaterialTheme.colorScheme.surfaceVariant
    }
    val textColor = try {
        Color(android.graphics.Color.parseColor(trip.routeTextColor))
    } catch (e: Exception) {
        MaterialTheme.colorScheme.onSurface
    }
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .fillMaxWidth()
            .height(72.dp)
            .padding(horizontal = 8.dp)
    ) {
        Box(
            contentAlignment = Alignment.Center,
            modifier = Modifier
                .height(40.dp)
                .widthIn(min = 64.dp)
                .background(color = tripColor, shape = MaterialTheme.shapes.medium)
        ) {
            Text(
                text = trip.routeShortName ?: "?",
                style = MaterialTheme.typography.titleLarge,
                color = textColor,
                fontWeight = FontWeight.Bold,
                modifier = Modifier.padding(horizontal = 12.dp)
            )
        }
        Spacer(Modifier.width(16.dp))
        Column {
            Row {
                Text(
                    text = trip.headSign ?: "?",
                    style = MaterialTheme.typography.bodyMedium
                )
                Spacer(Modifier.width(8.dp))
                Spacer(modifier = Modifier.weight(1f))
            }
            Text(
                text = "${stringResource(id = R.string.trip_id)} : ${trip.id ?: "?"}",
                style = MaterialTheme.typography.bodySmall
            )
        }
    }
}

@Composable
private fun DragHandle() {
    Surface(
        modifier = Modifier,
        color = MaterialTheme.colorScheme.surfaceVariant,
        shape = MaterialTheme.shapes.extraLarge
    ) {
        Box(
            Modifier
                .size(
                    width = 32.dp,
                    height = 4.dp
                )
        )
    }
}
