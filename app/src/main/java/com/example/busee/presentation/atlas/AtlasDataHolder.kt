package com.example.busee.presentation.atlas

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.RectF
import android.util.Log
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.toBitmap
import com.example.busee.BuildConfig
import com.example.busee.R
import com.example.busee.model.Agency
import com.example.busee.model.Coordinate
import com.example.busee.model.Region
import com.example.busee.model.Trip
import com.example.busee.model.Vehicle
import com.example.busee.source.TransitTrackerApi
import com.mapbox.geojson.Feature
import com.mapbox.geojson.FeatureCollection
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.serialization.json.Json
import org.maplibre.android.camera.CameraPosition
import org.maplibre.android.geometry.LatLng
import org.maplibre.android.maps.MapLibreMap
import org.maplibre.android.maps.MapView
import org.maplibre.android.style.expressions.Expression
import org.maplibre.android.style.expressions.Expression.gt
import org.maplibre.android.style.expressions.Expression.interpolate
import org.maplibre.android.style.expressions.Expression.literal
import org.maplibre.android.style.expressions.Expression.stop
import org.maplibre.android.style.expressions.Expression.zoom
import org.maplibre.android.style.layers.LineLayer
import org.maplibre.android.style.layers.PropertyValue
import org.maplibre.android.style.layers.SymbolLayer
import org.maplibre.android.style.sources.GeoJsonSource
import java.time.Instant
import java.util.concurrent.ConcurrentHashMap
import kotlin.time.Duration
import kotlin.time.Duration.Companion.minutes
import kotlin.time.DurationUnit
import kotlin.time.toDuration


class AtlasDataHolder(
    private val context: Context,
    private val transitTrackerApi: TransitTrackerApi
) {
    private val json = Json {
        ignoreUnknownKeys = true
        isLenient = true
        encodeDefaults = true
    }

    private var job: Job? = null

    var mapView: MapView? = null

    private val agencySlugMap: MutableMap<String, Agency> = ConcurrentHashMap()
    private val agencyVehicleMap: MutableMap<Agency, List<Vehicle>> = ConcurrentHashMap()

    private val _regionFlow: MutableStateFlow<Region?> = MutableStateFlow(null)
    val regionFlow = _regionFlow.asStateFlow()

    private val vehicleSource = GeoJsonSource(VEHICLE_SOURCE_ID, FeatureCollection.fromFeatures(emptyArray()))
    private val routeSource = GeoJsonSource(ROUTE_SOURCE_ID, FeatureCollection.fromFeatures(emptyArray()))

    private val _vehicleTypeFlow: MutableStateFlow<VehicleType> = MutableStateFlow(VehicleType.Both)
    val vehicleTypeFlow = _vehicleTypeFlow.asStateFlow()

    private val _selectedVehicleFlow: MutableStateFlow<Vehicle?> = MutableStateFlow(null)
    val selectedVehicleFlow = _selectedVehicleFlow.asStateFlow()

    private val _relatedTripsFlow: MutableStateFlow<List<Trip>> = MutableStateFlow(emptyList())
    val relatedTripsFlow = _relatedTripsFlow.asStateFlow()

    private val latestTimeFilterFlow: MutableStateFlow<Duration> = MutableStateFlow(20.minutes)

    fun setupMapView(regionSlug: String, mapView: MapView) {
        this.mapView = mapView
        job = Job()

        job?.let { job1 ->
            CoroutineScope(Dispatchers.Default + job1).launch {
                val region = transitTrackerApi.getRegion(regionSlug)
                _regionFlow.tryEmit(region)
                region ?: return@launch
                region.agencyList.forEach { agency -> agencySlugMap[agency.slug] = agency }
                CoroutineScope(Dispatchers.Main + job1).launch {
                    mapView.getMapAsync { mapLibre ->
                        mapLibre.centerRegion(mapCenter = region.mapCenter, zoom = region.mapZoom)
                        CoroutineScope(Dispatchers.Main + job1).launch {
                            mapLibre.setupStyle()
                            while (true) {
                                withContext(Dispatchers.IO) { loadVehicles(region) }
                                delay(60_000L)
                            }
                        }
                    }
                }
            }
        }

        mapView.getMapAsync { mapLibreMap ->
            mapLibreMap.addOnMapClickListener {
                Log.d(TAG, "onMapClick: $it")
                val screenCoordinate = mapLibreMap.projection.toScreenLocation(LatLng(it.latitude, it.longitude))
                val featureList = mapLibreMap.queryRenderedFeatures(RectF(screenCoordinate.x - 10f, screenCoordinate.y - 10f, screenCoordinate.x + 10f, screenCoordinate.y + 10f))
                featureList.firstOrNull { it.hasProperty(FeatureProperty.VehicleData.value) }?.let {
                    try {
                        Log.d(TAG, "feature: ${it.properties()}")
                        val vehicle = json.decodeFromString(Vehicle.serializer(), it.getStringProperty(FeatureProperty.VehicleData.value))
                        _selectedVehicleFlow.tryEmit(vehicle)
                        drawRouteShape(vehicle)
                        getRelatedTrips(vehicle)
                        Log.d(TAG, "vehicle: $vehicle")
                    } catch (e: Exception) {
                        Log.e(TAG, "Error decoding vehicle: $e")
                    }
                }
                true
            }
        }
    }

    fun clearMapView() {
        mapView = null
        job?.cancel()
    }

    private suspend fun loadVehicles(region: Region) {
        Log.d(TAG, "loadRegionAgencies")
        val newAgencyVehicleMap = region.agencyList.associateWith { agency -> transitTrackerApi.getVehiclesInAgency(agency) }
        agencyVehicleMap.clear()
        agencyVehicleMap.putAll(newAgencyVehicleMap)
        withContext(Dispatchers.Main) { drawVehicleMarkers(vehicleList = newAgencyVehicleMap.values.flatten()) }
    }

    private fun MapLibreMap.centerRegion(mapCenter: Coordinate, zoom: Double) {
        cameraPosition = CameraPosition.Builder()
            .target(LatLng(mapCenter.lat, mapCenter.lon))
            .zoom(zoom)
            .build()
    }

    private suspend fun MapLibreMap.setupStyle() {
        withContext(Dispatchers.Main) {
            setStyle("https://api.maptiler.com/maps/streets-v2/style.json?key=${BuildConfig.MAPTILER_API_KEY}") { style ->
                val busIcon = ResourcesCompat.getDrawable(context.resources, R.drawable.ic_bus, null)!!.apply { this.setTint(Color.White.toArgb()) }.toBitmap()
                val trainIcon = ResourcesCompat.getDrawable(context.resources, R.drawable.ic_train, null)!!.apply { this.setTint(Color.White.toArgb()) }.toBitmap()
                val vehicleBackgroundIcon = ResourcesCompat.getDrawable(context.resources, R.drawable.ic_sr_bus_bg_icon, null)!!.toBitmap()

                style.addImage(ICON_BACKGROUND, vehicleBackgroundIcon, true)
                style.addImage(ICON_BUS, busIcon)
                style.addImage(ICON_TRAIN, trainIcon)

                val vehicleSymbolLayer = SymbolLayer(VEHICLE_LAYER_ID, VEHICLE_SOURCE_ID).apply {
                    withProperties(
                        PropertyValue(
                            SymbolProperty.IconImage.value,
                            Expression.match(
                                Expression.get(FeatureProperty.VehicleType.value),
                                literal("bus"), literal(ICON_BUS),
                                literal(ICON_TRAIN)
                            )
                        ),
                        PropertyValue(
                            SymbolProperty.IconSize.value,
                            interpolate(
                                Expression.exponential(0.5f), zoom(),
                                stop(10f, 0.4f),
                                stop(14f, 0.71f)
                            )
                        ),
                        PropertyValue(SymbolProperty.Filter.value, gt(zoom(), literal(10.0))),
                        PropertyValue(SymbolProperty.IconAllowOverlap.value, true),
                    )
                }

                val vehicleBackgroundSymbolLayer = SymbolLayer(VEHICLE_BACKGROUND_LAYER_ID, VEHICLE_SOURCE_ID).apply {
                    withProperties(
                        PropertyValue(SymbolProperty.IconImage.value, ICON_BACKGROUND),
                        PropertyValue(
                            SymbolProperty.IconSize.value,
                            interpolate(
                                Expression.exponential(0.5f), zoom(),
                                stop(10f, 0.1f),
                                stop(14f, 0.61f)
                            )
                        ),
                        PropertyValue(SymbolProperty.IconAllowOverlap.value, true),
                        PropertyValue(SymbolProperty.IconColor.value, Expression.get(FeatureProperty.VehicleColor.value)),
                        PropertyValue(SymbolProperty.IconRotate.value, Expression.get(FeatureProperty.Direction.value))
                    )
                }

                val lineLayer = LineLayer(ROUTE_LAYER_ID, ROUTE_SOURCE_ID).apply {
                    withProperties(
                        PropertyValue(LineProperty.LineColor.value, Expression.get(FeatureProperty.RouteColor.value)),
                        PropertyValue(LineProperty.LineWidth.value, 4f)
                    )
                }

//                Maintain the order of adding layers and sources
                style.addLayer(lineLayer)
                style.addLayer(vehicleBackgroundSymbolLayer)
                style.addLayer(vehicleSymbolLayer)
                style.addSource(vehicleSource)
                style.addSource(routeSource)
            }
        }
    }

    @SuppressLint("NewApi")
    private fun drawVehicleMarkers(vehicleList: List<Vehicle>) {
        val filteredVehicleList = vehicleList
            .filter {
                it.isActive
            }
            .filter {
                val timeDiff = (Instant.now().epochSecond - it.timestamp).toDuration(DurationUnit.SECONDS)
                timeDiff < latestTimeFilterFlow.value
                true
            }
            .filter {
                when (vehicleTypeFlow.value) {
                    VehicleType.Bus -> it.vehicleType == "bus"
                    VehicleType.Train -> it.vehicleType == "train"
                    VehicleType.Both -> true
                }
            }
        val featureList = filteredVehicleList.map { vehicle ->
            Feature.fromGeometry(vehicle.location.point).apply {
                addStringProperty(FeatureProperty.VehicleType.value, vehicle.vehicleType)
                addStringProperty(FeatureProperty.VehicleColor.value, vehicle.trip.routeColor ?: "#000000")
                addStringProperty(FeatureProperty.ShapeId.value, vehicle.trip.shapeId)
                addStringProperty(FeatureProperty.VehicleData.value, json.encodeToString(Vehicle.serializer(), vehicle))
                vehicle.bearing?.let {
                    addNumberProperty(FeatureProperty.Direction.value, it)
                    addBooleanProperty(FeatureProperty.HasDirection.value, true)
                }
            }
        }

        val featureCollection = FeatureCollection.fromFeatures(featureList)
        vehicleSource.setGeoJson(featureCollection)
    }

    private fun drawRouteShape(vehicle: Vehicle) {
        job?.let { job1 ->
            CoroutineScope(Dispatchers.IO + job1).launch {
                vehicle.trip.shapeId ?: run {
                    withContext(Dispatchers.Main) { routeSource.setGeoJson(FeatureCollection.fromFeatures(emptyArray())) }
                    return@launch
                }
                val shape = transitTrackerApi.getTripShape(vehicle.agencySlug, vehicle.trip.shapeId)
                val featureCollection = FeatureCollection.fromJson(shape).apply {
                    features()?.firstOrNull()?.apply {
                        addStringProperty(FeatureProperty.RouteColor.value, vehicle.trip.routeColor ?: "#000000")
                    }
                }
                withContext(Dispatchers.Main) { routeSource.setGeoJson(featureCollection) }
            }
        }
    }

    private fun getRelatedTrips(vehicle: Vehicle) {
        job?.let { job1 ->
            CoroutineScope(Dispatchers.IO + job1).launch {
                vehicle.tripId ?: run {
                    _relatedTripsFlow.tryEmit(emptyList())
                    return@launch
                }
                val relatedTrips = transitTrackerApi.getRelatedTrips(vehicle.agencySlug, vehicle.tripId)
                _relatedTripsFlow.tryEmit(relatedTrips)
            }
        }
    }

    fun onVehicleSelected(vehicleType: VehicleType) {
        _vehicleTypeFlow.tryEmit(vehicleType)
        drawVehicleMarkers(agencyVehicleMap.values.flatten())
    }

    fun unselectVehicle() {
        _selectedVehicleFlow.tryEmit(null)
        _relatedTripsFlow.tryEmit(emptyList())
        routeSource.setGeoJson(FeatureCollection.fromFeatures(emptyArray()))
    }

    fun onTimeFilterChanged(timeFilter: Duration) {
        latestTimeFilterFlow.tryEmit(timeFilter)
        drawVehicleMarkers(agencyVehicleMap.values.flatten())
    }

    companion object {
        private const val TAG = "AtlasDataHolder"

        const val ICON_BACKGROUND = "icon-background"
        const val ICON_BUS = "icon-bus"
        const val ICON_TRAIN = "icon-train"
        const val ICON_DIRECTION = "icon-direction"

        const val VEHICLE_LAYER_ID = "vehicle-layer"
        const val VEHICLE_BACKGROUND_LAYER_ID = "vehicle-background-layer"
        const val VEHICLE_SOURCE_ID = "vehicle-source"

        const val ROUTE_SOURCE_ID = "route-source"
        const val ROUTE_LAYER_ID = "route-layer"

        enum class SymbolProperty(val value: String) {
            IconImage("icon-image"),
            IconSize("icon-size"),
            IconAllowOverlap("icon-allow-overlap"),
            IconColor("icon-color"),
            IconRotate("icon-rotate"),
            Filter("filter"),
        }

        enum class LineProperty(val value: String) {
            LineColor("line-color"),
            LineWidth("line-width")
        }

        enum class FeatureProperty(val value: String) {
            VehicleType("vehicle-type"),
            VehicleColor("vehicle-color"),
            ShapeId("shape-id"),
            VehicleData("vehicle-data"),
            RouteColor("route-color"),
            Direction("direction"),
            HasDirection("has-direction")
        }

        enum class VehicleType(val value: String) {
            Bus("bus"),
            Train("train"),
            Both("both")
        }
    }
}

