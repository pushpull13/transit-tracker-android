package com.example.busee.presentation

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.BackHandler
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.togetherWith
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import com.example.busee.model.Region
import com.example.busee.presentation.atlas.AtlasView
import com.example.busee.presentation.composable.InfoView
import com.example.busee.presentation.composable.SelectRegionView
import com.example.busee.presentation.theme.ANIMATION_DURATION
import com.example.busee.presentation.theme.BuseeTheme
import com.example.busee.source.TransitTrackerApi
import com.example.busee.utils.dataStore.DataStoreInstance
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.koin.android.ext.android.inject
import org.maplibre.android.MapLibre


class MainActivity : ComponentActivity() {

    //    private val atlasViewModel: AtlasViewModel by viewModel()
    private val dataStore: DataStoreInstance by inject()
    private val transitTrackerApi: TransitTrackerApi by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()

        MapLibre.getInstance(this@MainActivity)

//        /42.24165546/-83.0168559

        setContent {
            BuseeTheme {

                val selectedRegion by dataStore.region.collectAsState(initial = null)
                var showInfoView by remember { mutableStateOf(false) }
                BackHandler(showInfoView) { showInfoView = false }

                AnimatedContent(
                    targetState = selectedRegion,
                    transitionSpec = { fadeIn(tween(ANIMATION_DURATION)) togetherWith fadeOut(tween(ANIMATION_DURATION)) },
                    label = "regionSelection_animation"
                ) {
                    when (it) {
                        null -> CircularProgressIndicator()
                        "" -> {
                            var regionList by remember { mutableStateOf(emptyList<Region>()) }
                            LaunchedEffect(key1 = Unit) { withContext(Dispatchers.IO) { withContext(Dispatchers.Main) { regionList = transitTrackerApi.getAllRegions() } } }
                            SelectRegionView(
                                regionList = regionList
                            ) {
                                dataStore.putRegion(it.slug)
                            }
                        }

                        else -> AtlasView(regionSlug = it) { showInfoView = true }
                    }
                }
                AnimatedVisibility(
                    visible = showInfoView,
                    enter = fadeIn(tween(ANIMATION_DURATION)),
                    exit = fadeOut(tween(ANIMATION_DURATION))
                ) {
                    InfoView(onClickBack = { showInfoView = false })
                }
            }
        }
    }
}
