package com.example.busee.presentation.atlas

import android.util.Log
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.BottomSheetScaffold
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.SheetValue
import androidx.compose.material3.rememberBottomSheetScaffoldState
import androidx.compose.material3.rememberStandardBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshotFlow
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import com.example.busee.presentation.atlas.bottomSheet.VehicleBottomSheetContent
import com.example.busee.source.TransitTrackerApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.compose.koinInject
import org.maplibre.android.maps.MapView


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AtlasView(
    regionSlug: String,
    transitTrackerApi: TransitTrackerApi = koinInject(),
    onClickShowInfo: () -> Unit
) {
    val context = LocalContext.current
    val scope = rememberCoroutineScope()

    val atlasDataHolder = remember { AtlasDataHolder(context = context, transitTrackerApi = transitTrackerApi) }
    val mapView = atlasDataHolder.mapView

    val lifecycle = LocalLifecycleOwner.current.lifecycle
    DisposableEffect(lifecycle) {
        val observer = LifecycleEventObserver { _, event ->
            Log.d("LiveMapComposable", "LifecycleEventObserver: $event")
            when (event) {
                Lifecycle.Event.ON_START -> mapView?.onStart()
                Lifecycle.Event.ON_RESUME -> mapView?.onResume()
                Lifecycle.Event.ON_PAUSE -> mapView?.onPause()
                Lifecycle.Event.ON_STOP -> mapView?.onStop()
                Lifecycle.Event.ON_DESTROY -> {
                    atlasDataHolder.clearMapView()
                    mapView?.onDestroy()
                }

                else -> Unit
            }
        }
        lifecycle.addObserver(observer)
        onDispose {
            lifecycle.removeObserver(observer)
            atlasDataHolder.clearMapView()
        }
    }

    val region by atlasDataHolder.regionFlow.collectAsState()
    val vehicleType by atlasDataHolder.vehicleTypeFlow.collectAsState()
    val vehicle by atlasDataHolder.selectedVehicleFlow.collectAsState()
    val relatedTrips by atlasDataHolder.relatedTripsFlow.collectAsState()

    val bottomSheetState = rememberStandardBottomSheetState(initialValue = SheetValue.PartiallyExpanded, skipHiddenState = true)
    val bottomSheetScaffoldState = rememberBottomSheetScaffoldState(bottomSheetState = bottomSheetState)
    var isFullScreen by remember { mutableStateOf(false) }
    LaunchedEffect(Unit) {
        launch { snapshotFlow { bottomSheetScaffoldState.bottomSheetState.currentValue }.collect { isFullScreen = it == SheetValue.Expanded } }
    }

    fun expandBottomSheet() {
        scope.launch { bottomSheetScaffoldState.bottomSheetState.partialExpand() }
    }
    LaunchedEffect(vehicle) { if (vehicle != null) expandBottomSheet() }

    BackHandler(enabled = vehicle != null) { atlasDataHolder.unselectVehicle() }
    BackHandler(enabled = bottomSheetState.currentValue == SheetValue.Expanded) { scope.launch { bottomSheetState.partialExpand() } }

    BottomSheetScaffold(
        scaffoldState = bottomSheetScaffoldState,
        sheetSwipeEnabled = true,
        sheetPeekHeight = 164.dp,
        sheetShape = RectangleShape,
        sheetContainerColor = Color.Transparent,
        sheetShadowElevation = 0.dp,
        sheetDragHandle = null,
        sheetContent = {
            VehicleBottomSheetContent(
                vehicle = vehicle,
                relatedTrips = relatedTrips,
            )
        },
    ) {
        AndroidView(
            factory = { context ->
                MapView(context).apply {
                    atlasDataHolder.setupMapView(regionSlug = regionSlug, mapView = this)
                }
            },
            modifier = Modifier
                .fillMaxSize()
                .padding(bottom = 115.dp)
        )

        AtlasOverlay(
            visible = true,
            regions = region,
            vehicleType = vehicleType,
            onVehicleSelected = { atlasDataHolder.onVehicleSelected(it) },
            onClickShowInfo = onClickShowInfo
        )
    }
}
