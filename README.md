# Transit Tracker Android

This repository hosts the source code for the TransitTracker app, designed for real-time tracking of public transport locations. Users can access locations of buses and trains in real time, but it don't use this app for transportation purpose, information is not always be accurate.

Source code of this repo is lisenced under GNU Affero General Public License v3.0 

|![Screenshot 1](ss/ss1.png)|![Screenshot 1](ss/ss2.png)|
|--------------|-----------|
